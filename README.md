# README

Please include a 1-2 paragraph summary of your application’s architecture in the README (what your models will be, how they will relate to each other, routing, etc):

  My application consists of two linked resources (ActiveRecords), that are 
linked such that every event has many attendees. This is detailed in the /models 
folder. To keep things simple, I stripped out most of the default functionality given
by rails, and leave the user with the following pages

  / or /home : Homepage

  /about : Quick About Page

  /create : Create new Event Page

  /showevent/[URI] : Links to the event, from the URI that the user creates
  on the create page

  The hardest part was coding up the URI system, which ultimatly gives event privacy
and personalization of the links a user would send out to people for sign-in