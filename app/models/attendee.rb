class Attendee < ApplicationRecord
  belongs_to :event

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :name, presence: true, length: { maximum: 50 }
  #Next line makes sure that you can't check in twice to the same event
  validates_with NoDuplicateValidator
  validates :email, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }


end
