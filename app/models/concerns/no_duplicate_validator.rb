class NoDuplicateValidator < ActiveModel::Validator
  def validate(record)
    event = Event.find(record.event_id) 
    event.attendees.each do |a|
      if record.email.downcase == a.email.downcase
        record.errors.add(:email, "You can't check in twice to the same event (duplicate email)")
      end
    end
  end
end