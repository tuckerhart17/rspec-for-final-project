require 'date'

class TimeValidator < ActiveModel::Validator
  def validate(record)
    if record.start > record.end
      record.errors.add(:start, "Event must start before it ends")
    end

    if record.end < DateTime.now
      record.errors.add(:end, "Event must not end before present time")
    end
  end
end