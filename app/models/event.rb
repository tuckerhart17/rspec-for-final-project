class Event < ApplicationRecord
  has_many :attendees, dependent: :destroy

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :name, presence: true, length: { maximum: 50 }
  validates :location, presence:true, length: { maximum: 100 }
  validates_with TimeValidator
  validates :creator, presence: true, length: { maximum: 50 }
  validates :contact_email, length: { maximum: 255 },
                            format: { with: VALID_EMAIL_REGEX }
  validates :uri, presence: true, length: { maximum: 150 },
                            format: { with: /\A[a-zA-Z0-9]+\z/, message: "can't contain whitespace or invalid chars" }
  validates_with UniqueUri

end
