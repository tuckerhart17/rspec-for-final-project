json.extract! event, :id, :name, :location, :start, :end, :creator, :contact_email, :uri, :created_at, :updated_at
json.url event_url(event, format: :json)
