Rails.application.routes.draw do
  resources :attendees
  resources :events
  #For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/showevent/:uri', to: 'events#uri_find'
  get '/create', to: 'events#new'
  get '/checkin/:uri', to: 'attendees#checkin'
  get '/about', to: 'static#about'
  
  root 'static#home'
end
