class AddUriToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :uri, :string
  end
end
