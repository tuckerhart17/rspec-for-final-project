require 'rails_helper'
require './spec/helpers/event_helpers.rb'

#FROM https://gist.github.com/lbspen/5674563#file-gistfile1-txt-L4
def select_date(date, options = {})
  field = options[:from]
  base_id = find(:xpath, ".//label[contains(.,'#{field}')]")[:for]
  year, month, day = date.split(',')
  select year,  :from => "#{base_id}_1i"
  select month, :from => "#{base_id}_2i"
  select day,   :from => "#{base_id}_3i"
end

def select_time(hour, minute, options = {})
  field = options[:from]
  base_id = find(:xpath, ".//label[contains(.,'#{field}')]")[:for]
  select hour, :from => "#{base_id}_4i"
  select minute, :from => "#{base_id}_5i"
end
#END FROM


RSpec.feature "NewEvents", type: :feature do
  before :each do
    visit "/create"
  end

  scenario "User Vists creation page" do
    expect(page).to have_text("New Event")
  end

  scenario "User Creates a new event, and visits event page" do
    fill_fields

    click_button "Create Event"
    expect(page).to have_text("Event Created Successfully!")

    visit("/showevent/TestUri")
    expect(page).to have_text("Birthday Bash")
  end

  scenario "User Creates an event, and deletes it" do
    fill_fields
    
    click_button "Create Event"
    expect(page).to have_text("Event Created Successfully!")

    visit("/showevent/TestUri")
    expect(page).to have_text("Birthday Bash")

    click_link "Delete Event"

    expect(page).to have_text("Event was successfully destroyed.")
  end

  scenario "User attempts to visit an invalid page" do
    visit("/showevent/TestUri")
    expect(page).to have_text("New Event")
  end

  scenario "User provides a URI that's invalid" do
    fill_fields
    fill_in "Uri", :with => "Test Uri"
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides a Email that's invalid" do
    fill_fields
    fill_in "Contact email", :with => " _@@f"
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides a End time that's before the start time" do
    fill_fields
    select_date("2020,December,6", :from => "End")
    select_time("03", "00", :from => "End")
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides a Start time before the current time" do
    fill_fields
    select_date("2018,December,6", :from => "End")
    select_time("03", "00", :from => "End")
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides a Start time before the current time" do
    fill_fields
    select_date("2018,December,6", :from => "End")
    select_time("03", "00", :from => "End")
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides no event name" do
    fill_fields
    fill_in "Name", :with => ""
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User provides no Uri" do
    fill_fields
    fill_in "Uri", :with => ""
    click_button "Create Event"
    expect(page).to have_text("error")
  end

  scenario "User Event Name too long" do
    fill_fields
    fill_in "Name", :with => "a"*2000
    click_button "Create Event"
    expect(page).to have_text("error")
  end  
end