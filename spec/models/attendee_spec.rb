require 'rails_helper'

RSpec.describe Attendee, type: :model do
  before :each do
    @event = Event.first
    @attendee = Attendee.new(name: "Tucker Hart", email: "tuckerhart@yale.edu", event_id: @event.id)
  end

  it "is valid when valid args are passed" do
    expect(@attendee).to be_valid
  end

  it "has a name" do
    @attendee.name = "     "
    expect(@attendee).to be_invalid
  end

  it "has an email" do
    @attendee.email = "     "
    expect(@attendee).to be_invalid
  end

  it "has an appropriate length name" do
    @attendee.name = "a" * 51
    expect(@attendee).to be_invalid
  end

  it "has an appropriate length email" do
    @attendee.email = "a" * 244 + "@example.com"
    expect(@attendee).to be_invalid
  end

  it "is valid with valid emails" do
    valid_addresses = %w[attendee@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @attendee.email = valid_address
      expect(@attendee).to be_valid
    end
  end

  it "it is invalid with invlaid adresses" do
    invalid_addresses = %w[attendee@example,com attendee_at_foo.org attendee.name@example.
                           foo@bar_baz.com foo@bar+baz.com attendee@attendee@goog.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @attendee.email = invalid_address
      expect(@attendee).to be_invalid
    end
  end

  it "it is invalid when email is duplicate" do
    duplicate_attendee = @attendee.dup
    duplicate_attendee.email = @attendee.email.upcase
    @attendee.save
    expect(duplicate_attendee).to be_invalid
  end
  
end
