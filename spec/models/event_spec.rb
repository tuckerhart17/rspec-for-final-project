require 'rails_helper'

RSpec.describe Event, type: :model do
  before :each do
    @event = Event.new(name: "YoYo", location: "Paris", start: DateTime.now, end: DateTime.now + 1, creator: "Tucker", contact_email: "tucker.hart@yale.edu", uri: "momom")
  end

  it "is valid with valid values" do
    expect(@event).to be_valid
  end

  it "is invalid if name is not present" do
    @event.name = "     "
    expect(@event).to be_invalid
  end

  it "has creator" do
    @event.creator = "     "
    expect(@event).to be_invalid
  end

  it "has a start time that is before end time" do
    @event.end = @event.start - 1
    expect(@event).to be_invalid
  end

  it "has a uri present" do
    @event.uri = "     "
    expect(@event).to be_invalid
  end

  it "has an email present" do
    @event.contact_email = "     "
    expect(@event).to be_invalid
  end

  it "has a name that's an appropriate length" do
    @event.name = "a" * 51
    expect(@event).to be_invalid
  end

  it "email is appropriate size" do
    @event.contact_email = "a" * 244 + "@example.com"
    expect(@event).to be_invalid
  end

  it "accepts valid addresses" do
    valid_addresses = %w[event@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @event.contact_email = valid_address
      expect(@event).to be_valid
    end
  end

  it "regects invalid addresses" do
    invalid_addresses = %w[event@example,com event_at_foo.org event.name@example.
                           foo@bar_baz.com foo@bar+baz.com event@event@goog.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @event.contact_email = invalid_address
      expect(@event).to be_invalid
    end
  end

end
